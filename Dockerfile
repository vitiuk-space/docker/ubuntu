FROM phusion/baseimage:0.10.2

# Tell the container there is no tty
ENV DEBIAN_FRONTEND noninteractive

ENV DEFAULT_CONTAINER_TIMEZONE Europe/Kiev

# Automatic choose local mirror for sources list
COPY sources.list /etc/apt/sources.list
COPY . /app

# Update to latest packages and tidy up
RUN apt-get update && apt-get upgrade -y -o Dpkg::Options::="--force-confold" \
  && apt-get install --no-install-recommends -y tzdata iputils-ping \
  && apt-get -y autoremove \
  && apt-get -y clean \
  && apt-get purge -y --auto-remove \
  && rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/* \
  && chmod +x /app/bin/* \
  && ln -s /app/bin/set_timezone.sh /etc/my_init.d/00_set_timezone.sh
